import numpy as np

def sigmoid(x):
    return np.exp(x) / (1 + np.exp(x))

def softmax(x):
    x = x - np.max(x)
    y = np.exp(x) / np.sum(np.exp(x))
    return y

class GeneticAgent():
    def __init__(self, input_dim, output_dim, hid=[64,64,64], pop_size=4,\
            seed=1, discrete=False):

        self.input_dim = input_dim
        self.output_dim = output_dim
        self.hid = hid
        self.pop_size = pop_size
        self.seed = seed
        self.by = -0.00
        self.mut_noise = 1e-1
        self.discrete = discrete
        self.best_gen = -float("Inf")
        self.best_species = -float("Inf")
        np.random.seed(self.seed)
        self.init_pop()

    def get_action(self, obs, agent_idx=0, elite=True):

        my_agent = self.best_agent if elite else self.pop[agent_idx]

        x = obs        
        for ii in range(len(self.hid)):
            x = np.matmul(x, my_agent[ii])
            x[x<0] = 0 # relu

        # assume no batches for now (no need for axis in softmax) 
        p = softmax(self.by + np.matmul(x, my_agent[-1]))

        act = np.argmax(p, axis=-1)

        return act

    def get_fitness(self, env, render=False):
        fitness = []
        total_steps = 0

        for agent_idx in range(len(self.pop)):
            #obs = flatten_obs(env.reset())
            obs = env.reset()
            done=False
            accumulated_reward = 0.0
            
            while not done:
                if render: env.render(); time.sleep(0.05)
                action = self.get_action(obs, agent_idx=agent_idx, elite=False) 
                obs, reward, done, info = env.step(action[0])
                total_steps += 1
                accumulated_reward += reward
            render = False 
            fitness.append(accumulated_reward)
        plt.close("all")
        return fitness, total_steps
    
    def update_pop(self, fitness):

        # make sure fitnesses aren't equal
        fitness = fitness + np.random.randn(len(fitness),)*1e-6    
        sort_indices = list(np.argsort(fitness))
        sort_indices.reverse()

        sorted_fitness = np.array(fitness)[sort_indices]
        #sorted_pop = self.pop[sort_indices]

        
        if sorted_fitness[0] > self.best_species:
            self.best_agent = self.pop[sort_indices[0]]

        keep = int(np.ceil(0.1*self.pop_size))
        if np.mean(sorted_fitness[:keep]) > self.best_gen:
            print("new best elite population: {} v {}".\
                    format(np.mean(sorted_fitness[:keep]), self.best_gen))
            self.best_gen = np.mean(sorted_fitness[:keep])
            self.elite_pop = []
            for oo in range(keep):
                self.elite_pop.append(self.pop[sort_indices[oo]])
        else:
            # decay recollection of greatest generatio 
            self.elite_pop.insert(0,self.pop[sort_indices[0]]) 
            self.elite_pop.pop(-1)
            # only the best rep gets in
        #always keep the fittest individual
        self.pop = []
        num_elite = len(self.elite_pop)
        for pp in range(num_elite):
            self.pop.append(self.elite_pop[pp])

        for ll in range(keep,self.pop_size):
            new_layers = []
            for mm in range(len(self.hid)+1):
                rec_map = np.random.randint(num_elite, size=self.pop[0][mm].shape)
                new_layer = np.zeros_like(self.elite_pop[0][mm])
                for nn in range(num_elite):
                    new_layer = np.where(rec_map==nn, self.elite_pop[nn][mm],new_layer)
                new_layers.append(new_layer + self.mut_noise*np.random.randn(\
                        new_layer.shape[0], new_layer.shape[1]))


            self.pop.append(new_layers)
    
    def init_pop(self):
        # represent population as a list of lists of np arrays
        self.pop = []

        for jj in range(self.pop_size):
            layers = []
            xavier = 1.0 #(self.input_dim + self.hid[0]) / 2.0
            layer = np.random.randn(self.input_dim,self.hid[0]) / xavier
            #layer[np.abs(layer) < np.std(layer)] = 0
            layers.append(layer)
            for kk in range(1,len(self.hid)):
                    xavier = 1.0 #(self.hid[kk-1] + self.hid[kk])/2.0
                    layer = np.random.randn(self.hid[kk-1],self.hid[kk]) / xavier
                    #layer[np.abs(layer) < np.std(layer)] = 0
                    layers.append(layer)
            xavier = 1.0 # (self.hid[-1] + self.output_dim)/2.0
            layer = np.random.randn(self.hid[-1], self.output_dim) / xavier
            #layer[np.abs(layer) < np.std(layer)] = 0.0
            layers.append(layer)
            self.pop.append(layers)

        self.best_agent = self.pop[0]
