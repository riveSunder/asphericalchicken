import numpy as np
import time

import torch 
import torch.nn as nn
import torch.nn.functional as F

class QConvNet(nn.Module):

    def __init__(self, dim_in = (6,84,84), dim_act=9, seed = 0):
        super(QConvNet, self).__init__()

        conv_depth = 4
        kern_size = 3

        self.conv_block0 = nn.Sequential(\
                nn.Conv2d(dim_in[0], conv_depth, kern_size, 2),
                nn.LeakyReLU())
        # 40 x 40 x conv_depth
        self.conv_block1 = nn.Sequential(\
                nn.Conv2d(conv_depth, 2*conv_depth, kern_size, 2),\
                nn.LeakyReLU())

        # 18 x 18 x conv_depth*2
        self.conv_block2 = nn.Sequential(\
                nn.Conv2d(2*conv_depth, 4*conv_depth, kern_size, 2),\
                nn.LeakyReLU())

        #  7 x 7 x conv_depth*4
        self.conv_block3 = nn.Sequential(\
                nn.Conv2d(4*conv_depth, 8*conv_depth, kern_size, 2),\
                nn.LeakyReLU())

        #  4 x 4 x conv_depth*8
        self.dense = nn.Linear(conv_depth*128, dim_act)


    def forward(self,x):

        x = self.conv_block0(x)
        x = self.conv_block1(x)
        x = self.conv_block2(x)
        x = self.conv_block3(x)
        x = x.reshape(x.shape[0],-1)
        x = self.dense(x)

        return x

class QMLP(nn.Module):
    def __init__(self, dim_in = 324, dim_act=12, hid_dim= [512,128]):
        super(QMLP, self).__init__()

        self.dim_in = dim_in
        self.dim_act = dim_act
        self.hid_dim = hid_dim
        
        self.dense0 = nn.Linear(self.dim_in, self.hid_dim[0])
        self.dense1 = nn.Linear(self.hid_dim[0], self.hid_dim[1])
        self.dense2 = nn.Linear(self.hid_dim[1], self.dim_act)
        
    def forward(self, x):

        x = torch.selu(self.dense0(x))
        x = torch.selu(self.dense1(x))
        
        x = self.dense2(x)

        return x



if __name__ == "__main__":

    q = QConvNet()

    x = torch.randn(128,3,84,84)
    y = torch.randn(128,9)
    criterion = nn.MSELoss()
    optimizer = torch.optim.Adam(q.parameters(), lr=1e-3)

    for step in range(100):

        q.zero_grad()

        out = q.forward(x)
        loss = criterion(y, out)

        loss.backward()
        optimizer.step()
        if step % 10 == 0: print("convnet loss at step {} =  {}".format(step,loss))

    q_mlp = QMLP()
    x = torch.randn(128,324)
    y = torch.randn(128,12)
    criterion = nn.MSELoss()
    optimizer = torch.optim.Adam(q_mlp.parameters(), lr=1e-3)

    for step in range(100):

        q_mlp.zero_grad()

        out = q_mlp.forward(x)
        loss = criterion(y, out)

        loss.backward()
        optimizer.step()
        if step % 10 == 0: print("mlp loss at step {} =  {}".format(step,loss))
    
    print("decreasing losses indicate fitting a small random dataset")
