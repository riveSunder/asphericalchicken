import numpy as np
import time
import random
import copy 

import torch 
import torch.nn as nn
import torch.nn.functional as F

from chickens.dqn_models import QConvNet

from animalai.envs.gym.environment import AnimalAIEnv
from animalai.envs.arena_config import ArenaConfig

import gc


def create_env_fn(env_path, worker_id, arena_config_in, n_arenas=1):
    env = AnimalAIEnv(environment_filename=env_path,
                      worker_id=worker_id,
                      n_arenas=n_arenas,
                      arenas_configurations=arena_config_in,
                      docker_training=False,
                      retro=True)
    return env

class DQN(object):

    def __init__(self, env, dim_obs=(6,84,84), dim_act=9, batch_size=64, \
            epoch_size=1024, policy="conv"):
        # hard-coded (for now) hyperparameters
        self.eps_start = 0.95
        self.min_eps = torch.Tensor([0.05])
        self.eps_decay = torch.Tensor([0.995])
        self.lr = 1e-4
        self.max_buffer = 2048
        self.discount = 0.99

        self.replay_buffer = None
        self.batch_size = batch_size
        self.epoch_size = epoch_size
        self.env = env
        self.eps = torch.Tensor([self.eps_start]) 
        self.dim_obs = dim_obs
        self.dim_act = dim_act

        
        if policy == "conv":
            self._q = QConvNet(dim_in=self.dim_obs, dim_act=self.dim_act, seed=0)
            self._qt = QConvNet(dim_in=self.dim_obs, dim_act=self.dim_act, seed=0)
            
            self._qt.load_state_dict(copy.deepcopy(self._q.state_dict()))
            for param in self._qt.parameters():
                param.requires_grad = False
        else:
            self._q = QMLP() 

    def train(self, epochs=10):
        
        optimizer = torch.optim.Adam(self._q.parameters(), lr=self.lr)
        smooth_loss = None
        smoother = 0.99
        t0 = time.time()
        for epoch in range(epochs):

            # grab some episodes and stash in replay buffer
            self.episodes_to_buffer(steps=self.epoch_size)

            # get q loss and update our policy network q
            # sample a minibatch from replay buffer
            shuffle_idx = np.arange(len(self.replay_buffer["l_obs"]))
            np.random.shuffle(shuffle_idx)

            l_obs = self.replay_buffer["l_obs"][shuffle_idx]
            l_act = self.replay_buffer["l_act"][shuffle_idx]
            l_rew = self.replay_buffer["l_rew"][shuffle_idx]
            l_next_obs = self.replay_buffer["l_next_obs"][shuffle_idx]
            l_done  = self.replay_buffer["l_done"][shuffle_idx]

            for bstart in range(0, len(l_obs)-self.batch_size, self.batch_size):
                self._q.zero_grad()
                self._qt.zero_grad()
                loss = self.compute_q_loss(\
                        l_obs[bstart:bstart+self.batch_size],\
                        l_act[bstart:bstart+self.batch_size],\
                        l_rew[bstart:bstart+self.batch_size],\
                        l_next_obs[bstart:bstart+self.batch_size],\
                        l_done[bstart:bstart+self.batch_size])

                loss.backward()
                optimizer.step
                if smooth_loss is None:
                    smooth_loss = loss
                    print("initial smooth loss: ", smooth_loss)
                else: 
                    smooth_loss = smoother * smooth_loss + (1 - smoother) * loss

            # every once in a while, update our target network qt
            if epoch % 2 == 0:
                elapsed = time.time() - t0
                print("{} s epoch {} smooth loss {} current epsilon {}".format(\
                        elapsed, epoch, smooth_loss, self.eps))
                self.evaluate_policy()
                print("updating target network qt...")

                self._qt.load_state_dict(copy.deepcopy(self._q.state_dict()))
                for param in self._qt.parameters():
                    param.requires_grad = False

                gc.collect()

            # shuffle and truncate replay buffer 
            self.replay_buffer["l_obs"] = l_obs[:self.max_buffer]
            self.replay_buffer["l_act"] = l_act[:self.max_buffer]
            self.replay_buffer["l_rew"] = l_rew[:self.max_buffer]
            self.replay_buffer["l_next_obs"] = l_next_obs[:self.max_buffer]
            self.replay_buffer["l_done"] = l_done[:self.max_buffer]
        
            # update exploration factor epsilon
            self.eps = torch.max(self.eps * self.eps_decay, self.min_eps)
    def evaluate_policy(self, steps=1024):

        _, _, l_rew, _, l_done = self.get_episodes(steps)
        
        print("mean, reward: {}".format(torch.sum(l_rew)/torch.sum(l_done)))


    def episodes_to_buffer(self,steps=None):
        
        l_obs, l_act, l_rew, l_next_obs, l_done = self.get_episodes(steps)

        if self.replay_buffer is None:
            self.replay_buffer = {"l_obs": l_obs,\
                                    "l_act": l_act,\
                                    "l_rew": l_rew,\
                                    "l_next_obs": l_next_obs,\
                                    "l_done": l_done}
        else:
            temp_buffer = {"l_obs": l_obs,\
                                    "l_act": l_act,\
                                    "l_rew": l_rew,\
                                    "l_next_obs": l_next_obs,\
                                    "l_done": l_done}

            for name in self.replay_buffer:
                self.replay_buffer[name] = torch.cat(\
                        [self.replay_buffer[name], temp_buffer[name]], dim=0)


    def get_episodes(self, steps=None):

        if steps is None:
            steps = 1024
    
        # define tensors for replay buffer
        l_obs = torch.Tensor()
        l_rew = torch.Tensor()
        l_act = torch.Tensor()
        l_next_obs = torch.Tensor()
        l_done = torch.Tensor()

        done = True
        with torch.no_grad():
            for step in range(steps):
                if done:
                    _ = self.env.reset()
                    _, reward, done, info = self.env.step(0)
                    obs = self.get_obs_from_info(info)
                    done = False

                
                if torch.rand(1) < self.eps:
                    action = self.env.action_space.sample()
                else:
                    q_values = self._q(obs)
                    act = torch.argmax(q_values,dim=-1)
                    # detach action to send it to the environment
                    action = act.detach().numpy()[0]

                _, reward, done, info = self.env.step(action)

                # concatenate data from current step to buffers
                l_obs = torch.cat([l_obs, torch.Tensor(obs)], dim=0)
                l_rew = torch.cat([l_rew, torch.Tensor([reward])], dim=0)
                l_act = torch.cat([l_act, torch.Tensor([action])], dim=0)
                l_done = torch.cat([l_done, torch.Tensor([1.0*done]) ], dim=0)

                prev_obs = obs
                obs = self.get_obs_from_info(info, prev_obs)

                l_next_obs = torch.cat([l_next_obs, obs], dim=0)

                
        return l_obs, l_act, l_rew, l_next_obs, l_done

    def compute_q_loss(self, l_obs, l_act, l_rew, l_next_obs, l_done,\
            double=True):

        with torch.no_grad():
            qt = self._qt.forward(l_next_obs)
            if double:
                qtq = self._q.forward(l_next_obs)
                qt_max = torch.gather(qt, -1, torch.argmax(qtq, dim=-1).unsqueeze(-1))
            else:
                qt_max = torch.gather(qt, -1,  torch.argmax(qt, dim=-1).unsqueeze(-1))

            yj = l_rew + ((1-l_done) * self.discount * qt_max)

        l_act = l_act.long()
        q = self._q.forward(l_obs)
        q_act = q[..., l_act]


        loss =  torch.mean(torch.pow(yj-q_act,2))

        return loss

    def get_obs_from_info(self, info, prev_obs=None):

        brain_obj = info["brain_info"]

        vis_obs = torch.Tensor(brain_obj.visual_observations).squeeze()\
                .transpose(-3,-1).transpose(-2,-1).unsqueeze(0)

        # velocity vector not used
        #vector_obs = np.array(brain_obj.vector_observations).squeeze()
        if prev_obs is None:
            prev_obs = torch.zeros_like(vis_obs)
        # concatenate previous and current observation in the channels axis
        obs = torch.cat([vis_obs, prev_obs[:,:3,...]], dim=1)

        return obs

if __name__ == "__main__":
    env_path = '../AnimalAI-Olympics/env/AnimalAI'
    worker_id = random.randint(1, 100)
    arena_config_in = ArenaConfig('../AnimalAI-Olympics/examples/configs/2-Preferences.yaml')

    env = create_env_fn(env_path, worker_id, arena_config_in)
    if(0):
        import gym
        env = gym.make("CartPole-v0")

    dqn = DQN(env)

    dqn.train(epochs=100)
