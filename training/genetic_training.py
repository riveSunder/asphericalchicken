import numpy as np
import random
from chickens.genetic import GeneticAgent

from animalai.envs.gym.environment import AnimalAIEnv
from animalai.envs.arena_config import ArenaConfig


def create_env_fn(env_path, worker_id, arena_config_in, n_arenas=1):
    env = AnimalAIEnv(environment_filename=env_path,
                      worker_id=worker_id,
                      n_arenas=n_arenas,
                      arenas_configurations=arena_config_in,
                      docker_training=False,
                      retro=True)
    return env

def prepro_obs(info, prev_obs=None):

    dim = 12 
    brain_obj = info["brain_info"]

    vis_obs = np.array(brain_obj.visual_observations).squeeze()
    vector_obs = np.array(brain_obj.vector_observations).squeeze()
    new_vis = np.zeros((dim, dim, 3))
    scale = int(vis_obs.shape[0]/dim)
    
    for xx in range(dim):
        for yy in range(dim):
            for ch in range(3):
                new_vis[xx,yy,ch] = np.mean(vis_obs[xx*scale:(1+xx)*scale, yy*scale:(1+yy)*scale,ch])

    new_obs = np.append(new_vis.ravel(), vector_obs)


    if prev_obs is not None:
        prev_obs = prev_obs[dim*dim*3+3:]
        new_obs_dt = new_obs - prev_obs
    else: 
        new_obs_dt = (new_obs - np.min(new_obs)) / 255.

    return np.append(new_obs, new_obs_dt)

def get_fitness(agent, env, render=False):
    fitness = []
    total_steps = 0

    for agent_idx in range(len(agent.pop)):
        #obs = flatten_obs(env.reset())
        obs = env.reset()
        done=False
        accumulated_reward = 0.0
        o, reward, done, info = env.step(env.action_space.sample())

        obs = prepro_obs(info)

        while not done:
            obs = prepro_obs(info, obs)
            if render: env.render(); time.sleep(0.05)
            action = agent.get_action(obs, agent_idx=agent_idx, elite=False) 
            o, reward, done, info = env.step(action)
            total_steps += 1
            accumulated_reward += reward
        render = False 
        fitness.append(accumulated_reward)

    return fitness, total_steps
    
    
if __name__ == "__main__":

    env_path = '../AnimalAI-Olympics/env/AnimalAI'
    worker_id = random.randint(1, 100)
    arena_config_in = ArenaConfig('../AnimalAI-Olympics/examples/configs/1-Food.yaml')
    
    env = create_env_fn(env_path, worker_id, arena_config_in)
    o, reward, done, info = env.step(env.action_space.sample())

    act_dim = env.action_space.n
    obs = prepro_obs(info)

    obs_dim = obs.shape[0]

    agent = GeneticAgent(obs_dim, act_dim,pop_size=256)
    total_steps = 0
    max_steps = 1000

    gen = 0
    while gen < 100:#  total_steps < max_steps:

        fitness, steps = get_fitness(agent,env)
        f_record = [np.mean(fitness), np.std(fitness), np.max(fitness), np.min(fitness)]
        print("Gen. {} Reward Stats: MeanEp = {}, StdEp = {}, MaxEp = {}, MinEp = {}".format(gen, f_record[0], f_record[1], f_record[2], f_record[3]))
        agent.update_pop(fitness)

        total_steps += steps
        gen += 1
