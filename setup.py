from os.path import join, dirname, realpath
from setuptools import setup
import sys

setup(
    name='aSphericalChicken',
    py_modules=['chickens, training'],
    version='0.1',
    install_requires=[],
    description="training bots to be more animoid than animoid",
    author="Rive Sunder",
)
